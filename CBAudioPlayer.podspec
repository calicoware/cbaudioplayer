#
# Be sure to run `pod lib lint CBAudioPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'CBAudioPlayer'
s.version          = '1.1.2'
s.summary          = 'Simple wrapper for AVAudioPlayer'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = 'Wrapper for AVAudioPlayer which lets you use a completion handler to detect when the audio has finished playing.'

s.homepage         = 'https://bitbucket.org/calicoware/cbaudioplayer.git'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Chris Barker' => 'chrisbbarker@gmail.com' }
s.source           = { :git => 'https://bitbucket.org/calicoware/cbaudioplayer.git', :tag => s.version.to_s }
s.social_media_url = 'https://twitter.com/mrchrisbarker'

s.ios.deployment_target = '8.0'

s.source_files = 'CBAudioPlayer/Classes/**/*'
s.frameworks = 'AVFoundation'
end
