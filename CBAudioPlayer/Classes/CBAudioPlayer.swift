
//
//  CBAudioPlayer.swift
//  CBAudioPlayer
//
//  Created by Chris Brian Barker on 03/25/2017.
//  Copyright (c) 2017 Chris Brian Barker. All rights reserved.
//

import UIKit
import AVFoundation

public enum Status {
    case successful
    case prepareFail
    case decodeError
    case error
}

public class CBAudioPlayer: NSObject, AVAudioPlayerDelegate {

    var audioPlayer: AVAudioPlayer!
    
    var audioCompletion: (_ ss: Bool, _ status: Status) -> Void = {_,_ in }
    
    public var volume: Float {
        get { return audioPlayer.volume }
    }
    
    public var isPlaying: Bool {
        get { return audioPlayer.isPlaying }
    }
        
    public func pause() {
        guard let audio = audioPlayer else {
            return
        }
        audio.pause()
    }
    
    public func playAudioFile(url: URL, completion: @escaping (_ successful: Bool, _ status: Status) -> Void) {
        
        audioCompletion = completion
        
        do {
            audioPlayer = try AVAudioPlayer.init(contentsOf: url)
            audioPlayer.delegate = self
        }
        catch{
            audioCompletion(false, .error)
        }
        
        processAudioFile()
        
    }
    
    public func playAudioFile(data: Data, completion: @escaping (Bool, Status) -> Void) {
        
        audioCompletion = completion
        
        do {
            audioPlayer = try AVAudioPlayer.init(data: data)
            audioPlayer.delegate = self
        }
        catch{
            audioCompletion(false, .error)
        }
        
        processAudioFile()
        
    }
    
    internal func processAudioFile() {
        
        guard audioPlayer.prepareToPlay() == true else {
            audioCompletion(false, .prepareFail)
            return
        }
        
        audioPlayer.play()
    
    }
    
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        audioPlayer.stop()
        audioCompletion(flag, .successful)
    }
    
    public func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        audioPlayer.stop()
        audioCompletion(false, .decodeError)
    }
    
    
}
