# CBAudioPlayer

[![CI Status](http://img.shields.io/travis/Chris Brian Barker/CBAudioPlayer.svg?style=flat)](https://travis-ci.org/Chris Brian Barker/CBAudioPlayer)
[![Version](https://img.shields.io/cocoapods/v/CBAudioPlayer.svg?style=flat)](http://cocoapods.org/pods/CBAudioPlayer)
[![License](https://img.shields.io/cocoapods/l/CBAudioPlayer.svg?style=flat)](http://cocoapods.org/pods/CBAudioPlayer)
[![Platform](https://img.shields.io/cocoapods/p/CBAudioPlayer.svg?style=flat)](http://cocoapods.org/pods/CBAudioPlayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CBAudioPlayer is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CBAudioPlayer"
```

## Author

Chris Brian Barker, chris@calicoware.net

## License

CBAudioPlayer is available under the MIT license. See the LICENSE file for more info.
