//
//  ViewController.swift
//  CBAudioPlayer
//
//  Created by Chris Brian Barker on 03/25/2017.
//  Copyright (c) 2017 Chris Brian Barker. All rights reserved.
//

import UIKit
import CBAudioPlayer

class ViewController: UIViewController {

    let audioPlayer = CBAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func OnPlayURL(_ sender: Any) {
        
        guard let audioFile: URL = Bundle.main.url(forResource: "BrownFox",
                                                   withExtension: "m4a") else {
            return
        }
        
        audioPlayer.playAudioFile(url: audioFile) {
            (success, status) in
            
            if success {
                self.showMessage(message: "Audio played from URL()")
            }
            else{
                self.showMessage(message: "Audio failed from URL()")
            }
            
            print("Is playing: " + (self.audioPlayer.isPlaying ? "YES" : "NO"))
            
        }
        
        print("Is playing: " + (audioPlayer.isPlaying ? "YES" : "NO"))
        
    }

    @IBAction func OnPlayData(_ sender: Any) {
        
        guard let audioFile: URL = Bundle.main.url(forResource: "BrownFox",
                                                   withExtension: "m4a") else {
                return
        }
        
        let data: Data!
        
        do{
            data = try Data(contentsOf: audioFile)
        }
        catch{
            self.showMessage(message: "Audio failed from Data()")
            return
        }
        
        audioPlayer.playAudioFile(data: data) {
            (success, status) in
            
            if success {
                self.showMessage(message: "Audio played from Data()")
            }
            else{
                self.showMessage(message: "Audio failed from Data()")
            }
            
            print("Is playing: " + (self.audioPlayer.isPlaying ? "YES" : "NO"))
            
        }
        
        print("Is playing: " + (audioPlayer.isPlaying ? "YES" : "NO"))
        
    }
    
    private func showMessage(message: String) {
        let alert = UIAlertController(title: "CBAudioPlayer", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

